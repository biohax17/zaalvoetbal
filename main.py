import sys, warnings
if not sys.warnoptions:
    warnings.simplefilter("ignore")

import itertools
import pprint

from constraints import ConstraintEngine

import datetime

SEED = 1302

import random
#random.seed(18) #week
random.seed(SEED) #weekend

import pandas as pd
from pandas import ExcelWriter
from pandas import ExcelFile

from Anealler import LeagueSchedulingProblem

#teams = pd.read_excel('data/Weekcompetitie_voorkeuren.xlsx', na_filter=False).set_index("Team")
teams = pd.read_excel('data/Weekendcompetitie_voorkeuren.xlsx', na_filter=False).set_index("Team")
teams["Voorkeur dag"] = teams["Voorkeur dag"].str.lower()   # Some robustness

#zalen_df = pd.read_excel('data/Weekcompetitie_zalen.xlsx')
zalen_df = pd.read_excel('data/Weekendcompetitie_zalen.xlsx')


# Zalen toewijzen
def preproccess_location(locations):
    """ splits blocks of N hours in hourly blocks, used in preprocessing """
    #print locations.to_string()
    slots = pd.DataFrame(columns = ['time', 'location'])

    for index, row in locations.iterrows():
        date = datetime.datetime.strptime(row['Datum'], '%d/%m/%Y').date()
        start = datetime.datetime.strptime(row['Beginuur'], '%H:%M')
        hours = (datetime.datetime.strptime(row['Einduur'], '%H:%M') - start).seconds//3600

        for hour in range(hours):
            full_time = datetime.datetime.combine(date,
                                         (start + datetime.timedelta(hours=hour)).time())
            slots.loc[len(slots)] = [full_time, row["Locatie"]]     # Add a row

    return slots


slots = preproccess_location(zalen_df)
slots = pd.read_excel('data/Weekendcompetitie_zalen_minbeker.xlsx')


divided = teams.groupby('Divisie')

games = []
for name, division in divided:
    games.extend(list(itertools.permutations(division.index.tolist(), 2)))

random.shuffle(games)




def round_robin(units, sets = None):
    """ Generates a schedule of "fair" pairings from a list of units """
    if len(units) % 2:
        units.append(None)
    count = len(units)
    sets = sets or (count - 1)
    half = count / 2
    schedule = []
    for turn in range(sets):
        pairings = []
        for i in range(half):
            pairings.append((units[i], units[count-i-1]))
        units.insert(1, units.pop())
        schedule.append(pairings)
    return schedule



"""
# Generate games roundrobin per division
matchdays = {}
games = []
for name, division in divided:
    # Heen wedstrijden
    games = list(round_robin(division.index.tolist()))
    # Terug wedstijden
    ret = [[(game[1], game[0]) for game in matchday] for matchday in games]
    games.extend(ret)

    # Matchday samen van alle divisies
    for matchday, g in enumerate(games):
        if matchdays.has_key(matchday):
            matchdays[matchday].extend(g)
        else:
            matchdays[matchday] = g
    #pprint.pprint(games)

pprint.pprint(matchdays)

# Group weekends, weekend "aansnijden" per speeldag
# Grouping by consecutive days (weekends in this dataset)
weekends = iter(slots.groupby((slots.time.diff() > datetime.timedelta(days=1)).cumsum()))

# Initiele oplossing maken
columns = ["speeldag", "home", "away", "location", "time"]
schema = pd.DataFrame(columns=columns)

#TODO: heen en terug scheiden... 14/12
#1 en 4 zijn oneven (15 ploegen), 2, 3 en 5 zijn even (14 ploegen)

for matchday, games in matchdays.iteritems():
    try:
        weekend_slots = next(weekends)[1].reset_index()
        # if weekendslot insufficient => also take next weekend
        while len(weekend_slots) < len(games):
            weekend_slots = weekend_slots.append(next(weekends)[1], ignore_index=True)
    except:
        # weekends op   => random slots bezigen
        free_slots = slots[~slots.isin(schema[['time', 'location']]).all(1)]
        weekend_slots = free_slots.sample(n=len(games)).reset_index()

    for nr, game in enumerate(games):
        if not None in game:
            schema.loc[len(schema)] = [matchday, game[0], game[1],
                                       weekend_slots.loc[nr]["location"], weekend_slots.loc[nr]["time"]]
"""







"""
# Initiele oplossing maken
columns = ["speeldag", "home", "away", "location", "time"]
schema = pd.DataFrame(columns=columns)
selection = slots.sample(n=len(games), random_state=SEED).reset_index()

for nr, game in enumerate(games):
    schema.loc[len(schema)] = [0, game[0], game[1], selection.loc[nr]["location"], selection.loc[nr]["time"]]
"""
schema = pd.read_excel('data/Weekendcompetitie_dossche.xlsx')


print(schema.to_string())



lsp = LeagueSchedulingProblem(schema, slots, teams)
lsp.steps = 5000


state, e = lsp.anneal()
schedule = state["schedule"]
print("energy", e)

print(schedule.sort_values(['time', 'location']).to_string())

ce = ConstraintEngine(schedule, teams)
occurences = ce.count_conflicts()

print "Games in other half", occurences["separated_rounds"]
print "1 Game in weekend", lsp.karel(schedule) #TODO dyn print
print "Free day conflicts", occurences["free_days"]
print "Avoidable locations", occurences["avoid_loc"]
#print "Avoidable hours", lsp.avoid_time(schedule, "17:00", "22:00")
print("Avoidable hours zon", lsp.avoidable_hours(schedule, "zon", 10, 23))
print("Avoidable hours zat", lsp.avoidable_hours(schedule, "zat", 18, 23))
print("Avoidable hours zat", lsp.avoidable_hours(schedule, "vr", 20, 23))

print "Consecutive blocks", occurences["consecutive"]
print "Home preferred day", occurences["day_pref_home"]
print "Home preferred location", occurences["loc_pref_home"]
print "Away preferred day", occurences["day_pref_away"]
print "Away preferred location", occurences["loc_pref_away"]


