import collections
import datetime
import logging

import pandas as pd
from functools import partial


SEPARATOR = " - "


class ConstraintEngine():
    """Class combining all constraints"""

    # pass extra data (the distance matrix) into the constructor
    def __init__(self, schedule, teams):
        self.logger = logging.getLogger('annealer')
        self.logger.setLevel(logging.WARNING)
        # self.logger.setLevel(logging.INFO)
        self.schedule = schedule
        self.teams = teams

        self.functions = {
            "separated_rounds": self.separated_rounds,
            #"one_game_round": self.one_game_per_round,#TODO
            "free_days_home": partial(self.free_day, team="home"),
            "free_days_away": partial(self.free_day, team="away"),
            "avoid_loc": partial(self.avoid_loc, locations=["KTA", "HO"]), #TODO
            "avoid_loc1": partial(self.avoid_loc, locations=["SCH1", "SCH2", "SCH3"]), #TODO
            #"avoid_hours": self.avoidable_hours, #TODO
            "consecutive": self.non_consecutive_block,
            "day_pref_home": partial(self.day_preference, team="home"),
            "loc_pref_home": partial(self.location_preference, team="home"),
            "day_pref_away": partial(self.day_preference, team="away"),
            "loc_pref_away": partial(self.location_preference, team="away"),
        }



    def one_game_per_round(self, row, N=4):
        """ 1 wedstrijd in N aantal dagen"""
        conflicts = 0
        delta = datetime.timedelta(days=N)

        window = self.schedule[
                               (row['time'] <= self.schedule['time']) &
                               (self.schedule['time'] <= row['time'] + delta)]
        teams = window[['home', 'away']].values.flatten().tolist()
        for team, play_count in collections.Counter(teams).items():
            if play_count > 1:
                self.logger.info(team, "plays", play_count, "in", row['time'], row['time'] + delta)
                conflicts += 1

        return conflicts


    def free_day(self, row, team):
        """
        Vrije dag van zowel uit- als thuisteam respecteren
        :param str team: Home or away
        :return: if matchday is on free dayfor team
        :rtype: bool
        """

        time_as_str = row.time.strftime('%d/%m/%Y')

        # indien vrije dag van thuisploeg bezet is
        if time_as_str in self.teams.loc[getattr(row,team)]["Vrij op"].split(" - "):
            self.logger.info("conflict", getattr(row,team), time_as_str)
            return True


    def avoid_loc_OLD(self, schedule, locations):
        """TODO Decision to be made """
        conflicts = 0
        for loc in locations:
            conflicts += schedule[schedule.location == loc].shape[0]
        return conflicts

    def avoid_loc(self, row, locations):
        return row.location in locations

    def avoid_time_OLD(self, schedule, lower, upper):
        time_index = pd.DatetimeIndex(schedule['time'])
        # By setting start_time to be later than end_time, you can get the times that are not between the two times.
        # time_index.between_time(upper, lower, include_start=False, include_end=False).shape[0]
        return time_index.indexer_between_time(upper, lower).shape[0]

    def avoidable_hours(self, schedule, day, lower, upper):
        weekdays = {"ma": 0, "di": 1, "wo": 2, "do": 3, "vr": 4, "zat": 5, "zon": 6}
        return len(schedule.query("time.dt.dayofweek == {day} & (time.dt.hour<{lower} | time.dt.hour>{upper})"
                                  .format(day=weekdays[day], lower=lower, upper=upper)))

    def non_consecutive_block(self, row, nr=1):
        #TODO nr>1
        delta = datetime.timedelta(hours=nr)
        return self.schedule[(row.location == self.schedule.location) & \
                             (row.time < self.schedule.time) & (self.schedule.time <= row.time + delta)]\
                             .shape[0] == 0



    def day_preference(self, row, team):
        """
        :param str team: Home or away
        :return: if matchday is a preferred day for team
        :rtype: bool
        """
        weekdays = {0: "ma", 1: "di", 2: "wo", 3: "do", 4: "vr", 5: "zat", 6: "zon"}

        if weekdays[row.time.weekday()] in self.teams.loc[getattr(row,team)]["Voorkeur dag"].split(SEPARATOR):
            self.logger.info(getattr(row,team), "op", weekdays[row.time.weekday()],
                             "voorkeuren", self.teams.loc[getattr(row,team)]["Voorkeur dag"])
            return True


    def location_preference(self, row, team):
        """
        :param str team: Home or away
        :return: if matchday is a preferred location for team
        :rtype: bool
        """
        if row.location in self.teams.loc[getattr(row,team)]["Voorkeur zaal"].split(SEPARATOR):
            #TODO logcode
            return True


    def separated_rounds(self, row):
        split = datetime.datetime(year=2018, month=12, day=17)
        split2 = datetime.datetime(year=2018, month=12, day=31)

        # 1 en 4 zijn oneven (15 ploegen), 2, 3 en 5 zijn even (14 ploegen)
        teams = self.teams["Divisie"].value_counts()[self.teams.loc[row.home]["Divisie"]]
        if not teams % 2 == 0: teams += 1

        heen_matchdays = teams - 1
        # smaller than, starts with 0
        return (row.speeldag < heen_matchdays and row.time > split) \
            or (row.speeldag >= heen_matchdays and row.time < split2)


    def check_constraints_old(self, constraints=None):
        occurences = {
            "separated_rounds": 0,
            "one_game_round": 0,
            "free_days": 0,
            "avoid_loc": 0,
            "avoid_loc1": 0,
            "avoid_hours": 0, #TODO
            "consecutive": 0,
            "day_pref_home": 0,
            "loc_pref_home": 0,
            "day_pref_away": 0,
            "loc_pref_away": 0,
        }
        conflicts = {}

        for situation in occurences.keys():
            conflicts[situation] = pd.DataFrame(columns=self.schedule.columns)

        for row in self.schedule.itertuples():

            if self.separated_rounds(row): occurences["separated_rounds"] += 1
            if self.free_day(row, "home"): occurences["free_days"] += 1
            if self.free_day(row, "away"): occurences["free_days"] += 1

            if self.avoid_loc(row, ["KTA", "HO"]): occurences["avoid_loc"] += 1
            if self.avoid_loc(row, ["SCH1", "SCH2", "SCH3"]): occurences["avoid_loc1"] += 1

            if self.non_consecutive_block(row): occurences["consecutive"] += 1

            if self.location_preference(row, "home"): occurences["loc_pref_home"] += 1
            if self.day_preference(row, "home"): occurences["day_pref_home"] += 1
            if self.location_preference(row, "away"): occurences["loc_pref_away"] += 1
            if self.day_preference(row, "away"): occurences["day_pref_away"] += 1

        return occurences

    def find_conflicts(self, constraints=None, limit=None):
        # TODO query constraints with params
        constraints = constraints or self.functions.keys()
        conflicts = {}

        for situation in constraints:
            conflicts[situation] = pd.DataFrame(columns=self.schedule.columns)

        #for row in self.schedule.itertuples(): #TODO limit
        rows = self.schedule.itertuples()
        try:
            while not limit or sum([len(c) for c in conflicts.values()]) < limit:
                row = next(rows)
                for situation in constraints:
                    if self.functions[situation](row):
                        conflicts[situation] = conflicts[situation].append([row])
        except:
            pass  # iteration finished

        return conflicts

    def count_conflicts(self, constraints=None):
        return {k: len(v) for k,v in self.find_conflicts(constraints=constraints).iteritems()}
