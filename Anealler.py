import collections
import datetime
import logging
import math
import time
import random

import pandas as pd
from simanneal import Annealer

from constraints import ConstraintEngine


class LeagueSchedulingProblem(Annealer):

    """Annealer for AVZA competition.
    """

    # pass extra data (the distance matrix) into the constructor
    def __init__(self, init, slots, teams):
        self.logger = logging.getLogger('annealer')
        self.logger.setLevel(logging.WARNING)
        #self.logger.setLevel(logging.INFO)

        self.step = 0
        self.teams = teams
        self.slots = slots
        free_slots = slots[~slots.isin(init[['time','location']]).all(1)].reset_index()    # Difference

        state = {
            "schedule": init,
            "free_slots": free_slots
        }

        self.start_time = time.time()

        super(LeagueSchedulingProblem, self).__init__(state)  # important!

    def one_game_round(self, schedule, N=4):
        temp = schedule.set_index("time").sort_index().rolling('{days}d'.format(days=N))
        def duplicate_count(win):
            collections.Counter(win[['home', 'away']].values.flatten().tolist())

        duplicates = temp.apply(collections)
        print duplicates

    def karel(self, schedule):
        conflicts = 0
        day = datetime.timedelta(days=1)
        schedule = schedule.sort_values("time")

        # Grouping by consecutive days (weekends in this dataset)
        for weekend, games in schedule.groupby((schedule.time.diff() > day).cumsum()):
            teams = games[['home', 'away']].values.flatten().tolist()
            counts = collections.Counter(teams)
            conflicts += sum(counts.values()) - len(set(teams))   #occurences of teams - nr of teams gives duplicates

        return conflicts

    def move(self):
        """Swap timeslot of a game"""
        self.step += 1

        """
        #Handle hard constraint conflicts first
        ce = ConstraintEngine(self.state["schedule"], self.teams)
        conflicts = pd.DataFrame() # TODO double matchdays
        for row in self.state["schedule"].itertuples(): #TODO make get_conflicts method in constraint engine
            if ce.separated_rounds(row) or ce.free_day(row, "home") or ce.free_day(row, "away"):
                conflicts = conflicts.append(list(row))

        if (len(conflicts) > 0) and (random() > 0.5):
             a = random.choice(conflicts.index.tolist())

        else:
            a = random.randint(0, len(self.state["schedule"]) - 1) #Pure random
        
        b = random.randint(0, len(self.state["free_slots"]) - 1)



        #TODO nicer swap logic

        # Swap
        #temp_free_slots = self.free_slots.iloc[b][["time", "location"]].copy()
        #temp_state = self.state.iloc[a][["time", "location"]].copy()
        temp_free_slots = self.state["free_slots"].iloc[b][["time", "location"]].copy()
        temp_state = self.state["schedule"].iloc[a][["time", "location"]].copy()

        #raw_input("Press Enter to continue...")
        self.state["schedule"].set_value(a, 'time', temp_free_slots["time"])
        self.state["schedule"].set_value(a, 'location', temp_free_slots["location"])
        self.state["free_slots"].set_value(b, 'time', temp_state["time"])
        self.state["free_slots"].set_value(b, 'location', temp_state["location"])
        #.loc[, ["time", "location"]] = temp_free_slots
        #self.free_slots.loc[b, ["time", "location"]] = temp_state
        #self.state.loc[a, ["time", "location"]] = temp_free_slots
        #self.free_slots.loc[b, ["time", "location"]] = temp_state
        """

        #Only moves in same weekend
        # Handle hard constraint conflicts first
        ce = ConstraintEngine(self.state["schedule"], self.teams)
        conflict_dict = ce.find_conflicts(limit=20) # TODO double matchdays
        conflicts = pd.concat(conflict_dict.values())

        if (len(conflicts) > 0) and (random.random() > 0.5):
            conflicts.set_index(conflicts.columns[0], inplace=True)
            a = random.choice(conflicts.index.tolist())

        else:
            a = random.randint(0, len(self.state["schedule"]) - 1)  # Pure random

        match = self.state["schedule"].loc[a]
        match_weekend = self.slots[abs((self.slots.time - match.time).dt.days) < 4]

        try:
            to_swap = match_weekend.loc[random.choice(match_weekend.index.tolist())]

            schedule_slot = self.state["schedule"].query("location==@to_swap.location & time==@to_swap.time")
            if len(schedule_slot)>0: #if slot is already assigned
                schedule_slot_idx = schedule_slot.index[0]

                #Swap assigned slot
                self.state["schedule"].loc[schedule_slot_idx, ["location", "time"]] \
                                                      = match[["location", "time"]].copy()

            self.state["schedule"].loc[a, ["location", "time"]] = to_swap[["location", "time"]]
        except:
            print("error") #TODO eleganter afhandelen, geen idee wat er mis is

    def one_game_per_round(self, schedule):
        # TODO remove, in constraintengine
        # TODO param #Dagen (4D)
        conflicts = 0
        delta = datetime.timedelta(days=4)
        for idx, row in schedule.iterrows():
            window = schedule[(row['time'] <= schedule['time']) & (schedule['time'] <= row['time'] + delta)]
            teams = window[['home', 'away']].values.flatten().tolist()
            for x,y in collections.Counter(teams).items():
                if y>1:
                    self.logger.info(x, "plays",y, "in", row['time'],  row['time'] + delta)
                    conflicts += 1

        return conflicts


    def avoidable_hours(self, schedule, day, lower, upper):
        #TODO move to constraints
        weekdays = {"ma": 0, "di":1, "wo": 2, "do": 3, "vr": 4, "zat": 5, "zon": 6}
        return len(schedule.query("time.dt.dayofweek == {day} & (time.dt.hour<{lower} | time.dt.hour>{upper})"
                                  .format(day=weekdays[day], lower=lower, upper=upper)))


    def energy(self):
        """Objective function; cascade of priorities"""
        weigths = {
            "separated_rounds": self.state["schedule"].shape[0] ** 10 + 1,
            "one_game_round":   self.state["schedule"].shape[0] ** 9 + 1,
            "free_days":        self.state["schedule"].shape[0] ** 8 + 1,
            "avoid_loc":        self.state["schedule"].shape[0] ** 7 + 1,
            "avoid_loc1":       self.state["schedule"].shape[0] ** 6 + 1,
            "avoid_hours":      self.state["schedule"].shape[0] ** 5 + 1,
            "consecutive":      self.state["schedule"].shape[0] ** 4 + 1,
            "day_pref_home":    self.state["schedule"].shape[0] ** 3 + 1,
            "loc_pref_home":    self.state["schedule"].shape[0] ** 2 + 1,
            "day_pref_away":    self.state["schedule"].shape[0] ** 1 + 1,
            "loc_pref_away":    self.state["schedule"].shape[0] ** 0 + 1,
        }

        e = 0
        occurences = ConstraintEngine(self.state["schedule"], self.teams).count_conflicts()
        #e += occurences["separated_rounds"] * weigths["separated_rounds"]
        e += self.karel(self.state["schedule"]) * weigths["one_game_round"]

        e += occurences["free_days_home"] * weigths["free_days"]
        e += occurences["free_days_away"] * weigths["free_days"]

        e += occurences["avoid_loc"] * weigths["avoid_loc"]
        e += occurences["avoid_loc1"] * weigths["avoid_loc1"]
        # e += self.avoid_time(self.state["schedule"], "17:00", "22:00") * weigths["avoid_hours"]

        e += self.avoidable_hours(self.state["schedule"], "zon", 10, 23) * weigths["avoid_hours"]
        e += self.avoidable_hours(self.state["schedule"], "zat", 18, 23) * weigths["avoid_hours"]
        e += self.avoidable_hours(self.state["schedule"], "vr", 20, 23) * weigths["avoid_hours"]

        e += occurences["consecutive"] * weigths["consecutive"]
        e += (self.state["schedule"].shape[0] - occurences["day_pref_home"]) * weigths["day_pref_home"]
        e += (self.state["schedule"].shape[0] - occurences["loc_pref_home"]) * weigths["loc_pref_home"]
        e += (self.state["schedule"].shape[0] - occurences["day_pref_away"]) * weigths["day_pref_away"]
        e += (self.state["schedule"].shape[0] - occurences["loc_pref_away"]) * weigths["loc_pref_away"]

        if self.step % 50 == 0:
            print "Step", self.step
            print time.time() - self.start_time
            self.start_time = time.time()
            try:
                print self.best_energy
                self.best_state["schedule"].to_excel("Sol {step}_{steps}.xls".format(step=self.step, steps=self.steps))
            except:
                print self.state["schedule"].to_string()
                print e

        return e



    """
    0:     6912626305915225556170272 (Oplossing van uw vader)
    50:    6912616554202786893230129
    100:  
    
    """
